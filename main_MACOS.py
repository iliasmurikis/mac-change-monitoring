import os
import re
import sys
import time
import subprocess


def get_MAC():
    result = subprocess.run("netstat -nr | grep default",
                            shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
    ip = result.split('\n')[0].split()[1]
    result = subprocess.run("arp -a | grep " + ip,
                            shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')

    return result.split('\n')[0].split()[3]


def get_wifi_name():
    result = subprocess.run(
        "/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I",
        shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
    result = result.split('\n')
    result = [x.strip() for x in result]
    for x in result:
        if re.match(r"SSID:*", x):
            return x[6:]
    raise Exception("Couldn't find SSID")


def kill_net():
    subprocess.run('echo <password> | sudo -S networksetup -setnetworkserviceenabled Wi-Fi off', shell=True)


try:
    initial_wifi = get_wifi_name()
    initial_mac = get_MAC()
    print(initial_wifi, initial_mac)
    
    while 1:
        try:
            wifi = get_wifi_name()
            mac = get_MAC()

            # Checks if the MAC address is changed, under the same WiFi name
            if wifi == initial_wifi:
                if mac != initial_mac:
                    message = "You're exposed! \U0001F622 (new:%s)" % mac
                    kill_net()
                    print(message)
                else:
                    print('MAC is \u2705')

            # Repeat every 5 seconds
            time.sleep(5)
        except Exception as e2:
            print('Inside', e2)
except Exception as e:
    print('Outside', e)
