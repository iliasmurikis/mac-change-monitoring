import os
import re
import sys
import time
import subprocess


def get_MAC():
    result = subprocess.run("arping -f -I $(ip route show match 0/0 | awk '{print $5, $3}')",
                            shell=True, stdout=subprocess.PIPE).stdout.decode('utf-8')
    return re.findall("\[(.*?)\]", result)[0]


def get_wifi_name():
    result = subprocess.run("iwgetid", shell=True,
                            stdout=subprocess.PIPE).stdout.decode('utf-8')
    print(result)
    return re.findall('\"(.*?)\"', result)[0]


def write_message_to_file(text):
    # Write to output file
    with open(path + 'mac.sh', 'w+') as file:
        file.write("""echo "<span color='#9BF' weight='normal'><small><tt>%s</tt></small></span> | length=40" """ % text)


path = "./"
try:
    # Create the .sh file,
    open(path + 'mac.sh', 'a').close()
    # and make it executable.
    subprocess.run("chmod +x ./mac.sh", shell=True)

    initial_wifi = get_wifi_name()
    initial_mac = get_MAC()
    # Output initial MAC
    write_message_to_file(initial_mac + ' :white_check_mark:')
    while 1:
        try:
            wifi = get_wifi_name()
            mac = get_MAC()

            # Checks if the MAC address is changed, under the same WiFi name
            if (wifi == initial_wifi) and (mac != initial_mac):
                message = "You're exposed! :cry: (new:%s)" % mac
                print(message)
                write_message_to_file(message)

                # TODO: Think what to do next...
                time.sleep(30)

            # In case of new network connection, refresh...
            if mac != initial_mac:
                write_message_to_file(mac + ' :white_check_mark:')
                initial_mac = mac

            # Repeat every 10 seconds
            time.sleep(10)
        except Exception as e2:
            print('Inside', e2)
except Exception as e:
    print('Outside', e)
finally:
    # Delete the .sh file at the end of the program
    os.remove(path + 'mac.sh')
